;; Summary: Freefri -- an Emacs minor mode to dynamically set fringes
;; Creator: caseneuve
;; Last update: 2019-10-15, 20:07:00 @lenovo

;;;* VARIABLES
;;** custom fringe width
(make-variable-buffer-local
 (defvar freefri--custom-fringe-width nil
   "Special fringe width overriding fringes set by `freefri--set-local-fringes'."))

;;** divider vertical 
(make-variable-buffer-local
 (defvar freefri--div-vertical 0.1
   "Default divider used to calculate total fringe width when windows are split vertivally. E.g. 0.1 means that the frignes will take 10% of window width. Used by `freefri--set-local-fringes'."))

;;** divider horizontal
(make-variable-buffer-local
 (defvar freefri--div-horizontal 0.25
   "Default divider used to calculate total fringe width when windows are split horizontally. E.g. 0.1 means that the frignes will take 10% of window width. Used by `freefri--set-local-fringes'."))

;;** min window width 
(make-variable-buffer-local
 (defvar freefri--min-window-width 100
   "Minimum window width to add fringes. Used by `freefri--set-local-fringes'."))

;;** previous window width
(make-variable-buffer-local
 (defvar freefri--previous-window-width nil
   "Previous total window width."))


;;;* HELPER FUNCS
;;** is window split
(defun is-window-split ()
  "Non-nil if window is split -- \"vertical\" or \"horizontal\". Nil if there is one window in the frame."
  (let ((this-edges (window-edges (selected-window)))
        (next-edges (window-edges (next-window)))
        (howmany (length (window-list))))
    (if (> howmany 1)
        (if (= (car this-edges)
               (car next-edges))
            "horizontal" "vertical")
      nil)))

;;** reset local fringes
(defun freefri--reset-local-fringes (p)
  "Set local fringes to defaults set by `freefri--set-local-fringes' or set fringes to 0 when called with prefix argument P."
  (interactive "P")
  (let* ((divider (if (string= (is-window-split) "vertical")
                      freefri--div-vertical
                    freefri--div-horizontal))
         (fri (round (/ (* (window-pixel-width) divider) 2))))
    (if p (progn (setq freefri--custom-fringe-width 0)
                 (message "FREEFRI: fringes set locally to 0"))
      (progn (setq freefri--custom-fringe-width nil)
             (message "FREEFRI: fringes set locally to default %d" fri))))
  (set-window-buffer (selected-window) (current-buffer)))

;;** set local fringes
(defun freefri--set-local-fringes (window &optional arg)
  "Count the frame width of selected WINDOW according to minimum accepted window witdh set by `freefri--min-window-width' and dividers for vertical and horizontal split set by `freefri--div-horizontal' and `freefri--div-vertical'. Setting `freefri--custom-fringe-width' will override default actions of this function. With optional ARG increase or decrease (when ARG is < 0) fringes width."
  (with-current-buffer (buffer-name (window-buffer window))
  (unless (bound-and-true-p freefri--previous-window-width)
  (setq freefri--previous-window-width (window-total-width window)))
    (let* ((divider (if (string= (is-window-split) "vertical")
                        freefri--div-vertical
                      freefri--div-horizontal))
           (fri (if (and (= (window-total-width window) freefri--previous-window-width) 
                             freefri--custom-fringe-width)
                    freefri--custom-fringe-width
                  (round (/ (* (window-pixel-width window) divider) 2)))) 
           (n (if arg arg 0)))
      (if (and (> (window-total-width window) freefri--min-window-width)
               (not (minibufferp)))
          (progn
            (setq freefri--custom-fringe-width (+ fri n))
            (setq fringes-outside-margins t)
            (set-window-fringes window (+ fri n) (+ fri n) t))
        (set-window-fringes window 0 0 nil))
      (when arg
        (message "FREEFRI: fringes set to %s" freefri--custom-fringe-width)
        (set-window-buffer window (current-buffer))))
    (setq freefri--previous-window-width (window-total-width window))))

;;;* HOOK
;;** hook func
(defun freefri--hook-func ()
  (dolist (win (window-list))
    (unless (minibufferp (window-buffer (selected-window)))
      (freefri--set-local-fringes win))))

;;** add hook
;;;###autoload
(add-hook 'freefri-hook
          (lambda ()
            (if freefri
                (progn
                  (add-hook 'window-configuration-change-hook 'freefri--hook-func)
                  (add-function :before pre-redisplay-function
                                (lambda (_wins)
                                  (freefri--hook-func))))
              (remove-hook 'window-configuration-change-hook 'freefri--hook-func)
              (remove-function pre-redisplay-function
                               (lambda (_wins)
                                 (freefri--hook-func)))
              )))

;;;* DEFINE MINOR MODE 
;;;###autoload
(define-minor-mode freefri
  "Dynamically set fringe size."
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-+")
              (lambda () (interactive) (freefri--set-local-fringes
                                   (selected-window) 10)))
            (define-key map (kbd "C-_")
              (lambda () (interactive) (freefri--set-local-fringes
                                   (selected-window) -10)))
            (define-key map (kbd "C-)") 'freefri--reset-local-fringes)
            (define-key map (kbd "C-(")
              (lambda () (interactive) (freefri--reset-local-fringes 1)))
            map)
  :global t)

;;;* PROVIDE 
(provide 'freefri)
